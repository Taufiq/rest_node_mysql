Api Rest básica Que Hace CRUD sobre una Db MySQL, a una tabla llamada Costumers
Node.js
Express
MySQL

Para desplegar solo se entra al directorio Raíz y se ejecuta el comando:

------------------------------------------------------------------------------------

node server.js

GET     findAll:    http://localhost:3001/customers
GET     findOne:    http://localhost:3001/customers/:customerId
PUT     update:     http://localhost:3001/customers/:customerId
DELETE  delete:     http://localhost:3001/customers/:customerId
DELETE  deleteAll:  http://localhost:3001/customers

POST    create:     http://localhost:3001/customers/CustomerObj

    
    {
        "email": "jonathan@mail.com",
        "name": "Jonathan",
        "active": true
    }