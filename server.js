const express = require("express");
const bodyParser = require("body-parser");

const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}));

//simple route
app.get("/",(request,res)=>{
    res.json({messge:"Welcome to Jmarin API REST"})
} );


require("./app/routes/customer.routes.js")(app);
//set port, listen for request
app.listen(3001,()=>{
    console.log("Server corriendo en puerto 3001")
});
