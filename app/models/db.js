const mysql = require('mysql');
const dbConfig = require('../../config/db.config.js');

//Create comunication to the data base
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

//Open the MySQL connection 
connection.connect(error =>{
    if(error) throw error;
    console.log("Exitosa conexion a la base de datos");
});

module.exports = connection;